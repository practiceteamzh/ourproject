/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hibernateproject;


public class Service {
	StudentDao student=new StudentDaoImp();
	
	
	public void Create(Student s)
	{
		student.addStudent(s);
	}
	
	public void Update(Student s)
	{
		 s.setName("Michael");
	     student.updateStudent(s);
	}
	
	public void Delete(Student s)
	{
		  student.deleteStudent(s);
	}

}
