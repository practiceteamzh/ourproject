/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hibernateproject;


import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

import org.hibernate.Session;



public class StudentDaoImp implements StudentDao{
	
	

        @Override
	public List<Student> getAllStudents() {
		Session session = null;
	    List<Student> students = new ArrayList<>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      students = session.createCriteria(Student.class).list();
	    } catch (Exception e) {
	      JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'getAll'", JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return students;
	}

        @Override
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		  Session session = null;
	        try {
	            session = HibernateUtil.getSessionFactory().openSession();
	            session.beginTransaction();
	            session.save(student);
	            session.getTransaction().commit();
	            
	        	
	        	
	       
	        } catch (Exception ex) {
	        	JOptionPane.showMessageDialog(null, ex.getMessage(),"Ошибка при вставке", JOptionPane.OK_OPTION);

	        } finally {
	        	if (session != null && session.isOpen()) {

	                session.close();

	            } 
	        }
		//return null;
	}

        @Override
	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.update(student);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	}

        @Override
	public void deleteStudent(Student student) {
		// TODO Auto-generated method stub
		   Session session = null;
		    try {
		      session = HibernateUtil.getSessionFactory().openSession();
		      session.beginTransaction();
		      session.delete(student);
		      session.getTransaction().commit();
		    } catch (Exception e) {
		      JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при удалении", JOptionPane.OK_OPTION);
		    } finally {
		      if (session != null && session.isOpen()) {
		        session.close();
		      }
		    }
		
		
	}

	

        @Override
	public Student getStudent(int id) {
		
		 Session session = null;
		    Student student = null;
		    try {
		      session = HibernateUtil.getSessionFactory().openSession();
		      student = (Student) session.load(Student.class, id);
		    } catch (Exception e) {
		      JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		    } finally {
		      if (session != null && session.isOpen()) {
		        session.close();
		      }
		    }
		    return student;
	}





	
	

	
}
