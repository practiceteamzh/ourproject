/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hibernateproject;


import java.util.List;

public interface StudentDao {
	   public List<Student> getAllStudents();
	   public void addStudent(Student student);
	   public void updateStudent(Student student);
	   public void deleteStudent(Student student);
	   public Student getStudent(int id);

}
