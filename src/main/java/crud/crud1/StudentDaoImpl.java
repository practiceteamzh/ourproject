package crud.crud1;

import java.sql.SQLException;

public class StudentDaoImpl {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
	
		Student s=new Student();
		s.setName("Zhansaya");
		s.setId(1);

		StudentDAO student=new StudentDAO();
		
		//ADD
		student.insert(s);
		 
		//Update
		//student.alter(s);
		
		//Delete
		student.remove(s);
		
		
		
	}

}
