package crud.crud1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



public class DaoStudentImp implements DaoStudent{
	String url="jdbc:postgresql://postgresqltest:5432/Database1";
	String user="postgres";
	String pass="1234567890";
	 static List<Student> students = new ArrayList<Student>();
	

	public List<Student> getAllStudents() {
		 //List<Student> students = new ArrayList<Student>();
	        Connection con = null;
	        java.sql.Statement st = null;
	        ResultSet rs = null;
	     
	        try {
	            con = DriverManager.getConnection(url, user, pass);
	            st = con.createStatement();
	            rs = st.executeQuery("SELECT * FROM student");

	            	while(rs.next())
	        		{
	        			Student objStudent=new Student();
	        			objStudent.setId(rs.getInt("id"));
	        			objStudent.setName(rs.getString("name"));
	        			students.add(objStudent);
	        		}
	            

	        } catch (Exception ex) {
	            Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	            lgr.log(Level.SEVERE, ex.getMessage(), ex);

	        } finally {
	            try {
	                if (rs != null) {
	                    rs.close();
	                }
	                if (st != null) {
	                    st.close();
	                }
	                if (con != null) {
	                    con.close();
	                }

	            } catch (SQLException ex) {
	                Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	                lgr.log(Level.WARNING, ex.getMessage(), ex);
	            }
	        }
	        return students;
	}

	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		  Connection con = null;
		  PreparedStatement st = null;
		  ResultSet rs = null;
	        try {
	            //student.setLabelDate(new Date());
	        	
	            con = DriverManager.getConnection(url, user, pass);
	            //st = con.createStatement();
	            String sql="INSERT INTO student VALUES (?,?);";
	            st=con.prepareStatement(sql);
	            st.setString(2, student.getName());
	    		st.setInt(1, student.getId());
	            
	            st.execute();
	            
	    		
//	            dao.saveOrUpdate(topic);
	        } catch (SQLException ex) {
	            Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	            lgr.log(Level.SEVERE, ex.getMessage(), ex);

	        } finally {
	            try {
	                if (rs != null) {
	                    rs.close();
	                }
	                if (st != null) {
	                    st.close();
	                }
	                if (con != null) {
	                    con.close();
	                }

	            } catch (SQLException ex) {
	                Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	                lgr.log(Level.WARNING, ex.getMessage(), ex);
	            }
	        }
		//return null;
	}

	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		 Connection con = null;
		 PreparedStatement st = null;
	        ResultSet rs = null;
	        try {
	            //student.setLabelDate(new Date());
	        	
	            con = DriverManager.getConnection(url, user, pass);
	            //st = con.createStatement();
	            String sql="UPDATE student SET name=? WHERE id=?;";
	            st=con.prepareStatement(sql);
	            st.setString(1, student.getName());
	    		st.setInt(2, student.getId());
	            st.execute();
	    		
//	            dao.saveOrUpdate(topic);
	        } catch (SQLException ex) {
	            Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	            lgr.log(Level.SEVERE, ex.getMessage(), ex);

	        } finally {
	            try {
	                if (rs != null) {
	                    rs.close();
	                }
	                if (st != null) {
	                    st.close();
	                }
	                if (con != null) {
	                    con.close();
	                }

	            } catch (SQLException ex) {
	                Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
	                lgr.log(Level.WARNING, ex.getMessage(), ex);
	            }
	        }
		
	}

	public void deleteStudent(Student student) {
		// TODO Auto-generated method stub
		Connection con = null;
		 PreparedStatement st = null;
        ResultSet rs = null;
        try {
            //student.setLabelDate(new Date());
        	
            con = DriverManager.getConnection(url, user, pass);
            //st = con.createStatement();
            String sql="DELETE FROM student WHERE id=?;";
            st=con.prepareStatement(sql);
            st.setInt(1, student.getId());
            st.execute();
    		
//            dao.saveOrUpdate(topic);
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(DaoStudentImp.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		
		
	}

	

	public Student getStudent(int id) {
		
		return students.get(id);
	}





	
	

	
}
