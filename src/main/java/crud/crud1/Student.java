package crud.crud1;

import java.io.Serializable;

public class Student implements Serializable{
	private static final long serialVersionUID = 3717609684646312467L;
	private int Id;
	private String Name;
	
	public Student() {
		
	}
	
	public Student(int Id,String Name)
	{
		super();
		this.Id=Id;
		this.Name=Name;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (Id != other.Id)
			return false;
		return true;
	}
	
	
	
	

}
