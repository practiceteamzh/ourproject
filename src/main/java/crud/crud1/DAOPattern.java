package crud.crud1;

import javax.xml.parsers.FactoryConfigurationError;

public class DAOPattern {
	private Student objStudent;
	private StudentDAO objStudentDAO;
	
	public DAOPattern()
	{
		this.objStudent=new Student();
		this.objStudentDAO=new StudentDAO();
		
	}

	public Student getObjStudent() {
		return objStudent;
	}

	public void setObjStudent(Student objStudent) {
		this.objStudent = objStudent;
	}
	
	
	public String saveStudent()
	{
		if (this.objStudent.getId()!=0L) {
			try {
				this.objStudentDAO.alter(this.objStudent);
				System.out.println("Success");
				
				
			} catch (Exception e) {
				System.out.println("Error");
			}
			this.objStudent=new Student();
			
		}
		
		else 
		{
			
			try {
				this.objStudentDAO.insert(this.objStudent);
				System.out.println("Success");
				
			} catch (Exception e) {
				System.out.println("Error");
			}
			this.objStudent=new Student();

		}
		return "student.xhtml";
	}
	
	private String removeStudent()
	{
		try {
			this.objStudentDAO.remove(this.objStudent);
			System.out.println("Success");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		this.objStudent=new Student();
		return null;
	}
}

